# vant3-form-viewer
# 基于 vue3 和 vant3 的表单展现器，结合 [element-plus-form-designer](https://gitee.com/loogn/element-plus-form-designer).

包含两个组件 FormRenderer 和 FormViewer.
FormRenderer 组件用于呈现表单组件和获取表单提交数据，FormViewer 组件查看数据。

```
npm i vant3-form-viewer
```

## FormDesigner 使用
```html
<script setup>

import { reactive, ref } from 'vue';
import {FormRenderer} from 'vant3-form-viewer';

let formData = reactive({
  "controls": [
    {
      "type": "upload",
      "name": "文件上传",
      "key": "B7C0vljoo",
      "id": "upload_B7C0vljoo",
      "lock": false,
      "dataType": "string",
      "props": {
        "width": 12,
        "showLabel": true,
        "labelWidth": null,
        "label": "文件上传",
        "defaultValue": [],
        "buttonText": "点击上传",
        "sizeUnit": "MB",
        "size": 5,
        "required": false,
        "requiredMessage": "必填字段",
        "multiple": false,
        "withCredentials": false,
        "showFileList": true,
        "accept": ".jpeg,.jpg,.png,.json",
        "limit": 2,
        "disabled": false,
        "tip": "请上传文件",
        "customClass": ""
      },
      "rules": [
        {
          "message": "必填字段",
          "required": true
        }
      ]
    },
    {
      "type": "uploadImage",
      "name": "图片上传",
      "key": "d5CodocO2",
      "id": "uploadImage_d5CodocO2",
      "lock": false,
      "dataType": "string",
      "props": {
        "width": 12,
        "showLabel": true,
        "labelWidth": null,
        "label": "图片上传",
        "defaultValue": [],
        "sizeUnit": "MB",
        "size": 5,
        "required": false,
        "requiredMessage": "必填字段",
        "multiple": false,
        "withCredentials": false,
        "showFileList": true,
        "accept": ".jpg,.png",
        "limit": 1,
        "disabled": false,
        "tip": "",
        "customClass": ""
      },
      "rules": [
        {
          "message": "必填字段",
          "required": true
        }
      ]
    },
  ],
  "props": {
    "labelPosition": "right",
    "labelWidth": 100,
    "size": "default",
    "customClass": "",
    "cols": 12
  },
});
let formModel = reactive({
  upload_B7C0vljoo: [],
  uploadImage_d5CodocO2: [],
});

let uploadOptions = {
  action: 'http://localhost:8888//UploadFile',
  getHeaders: function () {
    return { 'token': '123456' };
  },
  getFileHook: function (res) {
    if (res.success) {
      return {
        name: res.result.url.substr(res.result.url.lastIndexOf('/') + 1),
        url: res.result.url
      };
    } else {
      return res.msg;
    }
  }
}
let formRenderer = ref(null);
function onSubmit(values) {
  console.log("values", values);//提交数据
}
</script>

<template>
  <FormRenderer
    ref="formRenderer"
    @submit="onSubmit"
    :uploadOptions="uploadOptions"
    :formData="formData"
    :formModel="formModel"
  />

  <van-button round block type="primary" @click="formRenderer.submit()">提交</van-button>
</template>

```


uploadOptions说明：
- action 表单内上传组件的上传地址。
- getHeaders 一个方法，返回自定义上传头内容。
- getFileHook 一个方法，自定义从action返回结果中获取文件信息，成功返回  ```{name:'文件名称',url:'文件地址'}```，失败返回错误信息字符串。  


FormRenderer 公开的 van-form 的几个方法：
- validate  对整个表单作验证。 
- submit   提交表单
- scrollToField  滚动到指定表单字段


## FormViewer 使用

属性和 FormRenderer 相同。

## 联系

邮箱：loogn_0707#126.com ，#自行改为@
