function upload(action, headers, file) {

    return new Promise((resolve, reject) => {
        //初始化
        const xhr = new XMLHttpRequest();
        xhr.open('POST', action, true);
        for (const key in headers) {
            if (Object.hasOwnProperty.call(headers, key)) {
                const element = headers[key];
                xhr.setRequestHeader(key, element);
            }
        }
        xhr.responseType = 'json';

        const genericErrorText = `上传文件失败: ${file.name}.`;
        xhr.addEventListener('error', () => reject(genericErrorText));
        xhr.addEventListener('abort', () => reject());
        xhr.addEventListener('load', () => {
            
            resolve(xhr.response);
        });
        //发送
        const data = new FormData();
        data.append('file', file);
        xhr.send(data);
    });
}
export { upload };