
class BaseControl {
    constructor(type, name) {
        this.type = type;
        this.name = name;
        this.key ='';
        this.id = type + "_" + this.key;
        this.lock = false;
    }
}

export default BaseControl;